﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.Azure.Storage; // Namespace for CloudStorageAccount
using Microsoft.Azure.Storage.Queue; // Namespace for Queue storage types

namespace Deque
{
    class Program
    {
        static async Task Main(string[] args)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new
                Microsoft.Azure.Storage.Auth.StorageCredentials("vladdana",
                "94yF6e+rNTzCL91xwS1OAaZ77EqOCXjKBX2JqA7TuzswjKQpMg2et1faAPfOqcSKhY32AIPbfBqmKKAFR2WKLA=="),
                true);

            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            CloudQueue queue = queueClient.GetQueueReference("myqueue7");

            //queue.CreateIfNotExists();

            

            while (true)
            {
                string tweetText = await receiveMessageAsync(queue);
                await connectToFlaskAndDatabase(tweetText);            
               
            }

            
            
            Console.ReadLine();
        }

        static async Task<string> receiveMessageAsync(CloudQueue cloudQueue)
        {
            while (true)
            {
                CloudQueueMessage retrievedMessage = await cloudQueue.GetMessageAsync();

                if (retrievedMessage != null)
                {
                    var theMessage = retrievedMessage.AsString;
                   // var tweetDate = retrievedMessage.InsertionTime;
                    return theMessage;
                }

            }

        }

        static async Task connectToFlaskAndDatabase(string tweetText)
        {
            using (TweetEntities context = new TweetEntities())
            {
                HttpClient httpClient = new HttpClient();
                var content = new StringContent("{\"text\":\"" + tweetText + "\"}", Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync("http://127.0.0.1:5003/predict", content);
                var responseString = await response.Content.ReadAsStringAsync();
                var responseDouble = JsonConvert.DeserializeObject<double>(responseString);

                var tweet = new Tweets { Value = tweetText, Sentiment = responseDouble, Date = DateTime.Now };

                //var tweet = new models.Tweet { Value = tweetText, Sentiment = responseDouble, Date = DateTime.Now };
                //Console.WriteLine(tweet.Value + " " + tweet.Sentiment);
                if (context.Tweets.Any(t => t.Value == tweet.Value)) return;

                context.Tweets.Add(tweet);
                context.SaveChanges();
            }
        }
    }
}
