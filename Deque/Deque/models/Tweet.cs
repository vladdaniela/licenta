﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deque.models
{
    class Tweet
    {
        
        public int TweetId { get; set; }
        public DateTime Date { get; set; }
        public string Value { get; set; }
        public double Sentiment { get; set; }
    }
}
