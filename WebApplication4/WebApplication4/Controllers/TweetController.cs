﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Data;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [Authorize]
    public class TweetController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext context;

        public TweetController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SerachForTweets(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Project with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            IEnumerable<Keyword> keywords = new List<Keyword>();
            if (project != null)
            {
                keywords = context.Keywords.Where(x => x.ProjectId == project.ProjectId);
            }

            double negativeTweets = 0;
            double positiveTweets = 0;
            double neutralTweets = 0;
            double total = 0;
            IEnumerable<Tweet> tweets = new List<Tweet>();
            DateTime[] labels= { };
            double[] indexes= { };
            for (int i = 0; i < keywords.Count(); i++)
            {
                if (keywords.ElementAt(i).Active == true)
                {
                    tweets = context.Tweets.Where(x => x.Value.Contains(keywords.ElementAt(i).Word)).AsEnumerable();
                    labels = new DateTime[tweets.Count()];
                    indexes = new double[tweets.Count()];
                    for (int j = 0; j < tweets.Count(); j++)
                    {
                        labels[j] = tweets.ElementAt(j).Date;
                        indexes[j] = tweets.ElementAt(j).Sentiment;
                        if (tweets.ElementAt(j).Sentiment == 1)
                        {
                            positiveTweets++;
                            total++;
                        }
                        else if (tweets.ElementAt(j).Sentiment == 0)
                        {
                            neutralTweets++;
                            total++;
                        }
                        else if (tweets.ElementAt(j).Sentiment == -1)
                        {
                            negativeTweets++;
                            total++;
                        }
                        Keyword_Tweet connectionTable = new Keyword_Tweet
                        {
                            Keyword = keywords.ElementAt(i),
                            KeywordId = keywords.ElementAt(i).KeywordId,
                            Tweet = tweets.ElementAt(j),
                            TweetId = tweets.ElementAt(j).TweetId,                         
                        };
                        context.Keyword_Tweets.Add(connectionTable);
                        context.SaveChanges();
                    }
                }
            }

            double negativeTweetsPercent = 0;
            double positiveTweetsPercent = 0;
            double neutralTweetsPercent = 0;


            if (negativeTweets > 0)
            {
                negativeTweetsPercent = (negativeTweets / total) * 100;
            }

            if (positiveTweets > 0)
            {
                positiveTweetsPercent = (positiveTweets / total) * 100;
            }

            if (neutralTweets > 0)
            {
                neutralTweetsPercent = (neutralTweets / total) * 100;
            }

           

            var model = new SearchForTweetsModel
            {
                ProjectId = project.ProjectId,
                PositiveTweets = Math.Round(positiveTweetsPercent, 0),
                NeutralTweets = Math.Round(neutralTweetsPercent, 0),
                NegativeTweets = Math.Round(negativeTweetsPercent, 0),
                Sentence = project.Sentence,
                Labels = labels,
                TweetSentiments = indexes
            };

            setOpinion(model);

            return View(model);
        }



        public void setOpinion(SearchForTweetsModel model)
        {
            if (model.PositiveTweets > model.NegativeTweets && model.PositiveTweets > model.NeutralTweets || model.NegativeTweets == model.NeutralTweets)
            {
                model.Opinion = "It seems like your product is on a good way. Many people tweeted positive things about it and we all know how important is their opinion. This product has a very good place when it comes to human pleasures. I think it deserves a star from me. Go search for another one! I hope it will be at least as good as this one.";
            }
            else if (model.NegativeTweets > model.PositiveTweets && model.NegativeTweets > model.NegativeTweets || model.PositiveTweets == model.NeutralTweets)
            {
                model.Opinion = "Meeeh, not such a good choice :(. This product is not well seen in people's eyes. They did not tweet so many good things about it. There are more bad comment than good ones. I hope this will not dissapoint you. Go try for another product! Maybe there will be a better one.";
            }
            else if (model.NeutralTweets > model.NegativeTweets && model.NeutralTweets > model.PositiveTweets || model.PositiveTweets == model.NegativeTweets)
            {
                model.Opinion = "Hmmmm, I don't know what to say. It seems like opinions are divided. Tweets are impartial in this context. Maybe in the future we will have a much more precise view. Go try for another one and for this product I think you should wait a little more in order for the people to decide if it is worth it or not.";
            }
        }

        [HttpGet]
        public IActionResult Back(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);

            return Redirect("/Project/Details/"+project.ProjectId);
        }


      
    }
}