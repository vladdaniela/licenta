﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Data;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext context;

        public ProjectController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.context = context;
        }

        public IActionResult DeleteProject(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);
            string userId = project.UserId;
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Project with id ={id} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                foreach (var word in context.Keywords.Where(x => x.ProjectId == project.ProjectId))
                {
                    context.Keywords.Remove(word);
                }
                context.Projects.Remove(project);
                context.SaveChanges();

                if (context.Projects.FirstOrDefault(x => x.ProjectId == project.ProjectId) == null)
                {
                    return Redirect("/Account/MyProfile/" + userId);
                }

                return RedirectToAction("Index", "Home");

            }
        }

        [HttpGet]
        public IActionResult EditProject(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Project with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            var model = new EditProjectViewModel
            {
                Id = project.ProjectId,
                Name = project.Name,
                UserId = project.UserId
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditProject(EditProjectViewModel model)
        {
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == model.Id);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Project with id = {model.Id.ToString()} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                project.Name = model.Name;

                context.Projects.Update(project);
                context.SaveChanges();
                if (context.Projects.FirstOrDefault(x => x.ProjectId == model.Id&&x.Name==model.Name)!=null)
                {
                    return Redirect("/Account/MyProfile/"+project.UserId);
                }                
            }

            return View(model);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AddProject(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            CreateProjectViewModel project = new CreateProjectViewModel { UserId = user.Id};

            return View(project);
        }

        [HttpGet]
        public IActionResult Details(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Project with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            var model = new DetailsViewModel
            {
                Id = project.ProjectId,
                Name = project.Name,
                DateAdded=project.DateAdded,
                DateModified=project.DateModified,
            };

            IEnumerable<Keyword> keywords = new List<Keyword>();
            if (model.Id>0)
            {
                keywords = context.Keywords.Where(x => x.ProjectId == model.Id);
            }

            model.Keywords = keywords;

            string sentence = null;
            if (model.Keywords.Count() > 0)
            {            
                for (int i = 0; i < model.Keywords.Count(); i++)
                {
                    if (model.Keywords.ElementAt(i).Active == true)
                    {
                        sentence += model.Keywords.ElementAt(i).Word+" ";
                    }
                }
            }
            model.Sentence = sentence;
            project.Sentence = model.Sentence;
            context.Projects.Update(project);
            context.SaveChanges();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddProject(CreateProjectViewModel model)
        {
         
            var user = await userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with id = {model.UserId} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var project = new Project
                    {
                        Name = model.Name,
                        UserId = model.UserId,
                        User = user,
                        DateAdded = DateTime.Now,
                        DateModified = DateTime.Now
                    };

                    await context.Projects.AddAsync(project);
                    await context.SaveChangesAsync();

                    if (project == null)
                        ViewData["notCreated"] = "Project not created successfully!";
                    else if (project != null)
                        return Redirect("/Project/Details/" + project.ProjectId);
                }                    
                  
            }
            return View(model);
        }
    }
}