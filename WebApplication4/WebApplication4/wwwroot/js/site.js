﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var canvas = document.querySelector('canvas');

var c = canvas.getContext('2d');



var mouse = {
    x: undefined,
    y: undefined
}

canvas.width = window.innerWidth - 17;
canvas.height = window.innerHeight - 155;


window.addEventListener("resize", function () {
    canvas.width = window.innerWidth - 17;
    canvas.height = window.innerHeight - 155;
    init();
}, false);

window.addEventListener('mousemove', function (event) {
    mouse.x = event.x;
    mouse.y = event.y;

});



var maxRadius = 40;
//var minRadius = 10;
var colorArray = [
    '#F25CA2',
    '#0433BF',
    '#032CA6',
    '#021859',
    '#0B9ED9'
];

function Circle(x, y, dx, dy, radius) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.minRadius = radius;
    this.color = colorArray[Math.floor(Math.random() * colorArray.length)];

    this.draw = function () {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();

    }

    this.update = function () {
        if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
            this.dx = -this.dx;
        }

        if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
            this.dy = -this.dy;
        }

        this.x += this.dx;
        this.y += this.dy;

        if (mouse.x - this.x < 50 && mouse.x - this.x > -50
            && mouse.y - this.y < 50 && mouse.y - this.y > -50) {
            if (this.radius < maxRadius) {
                this.radius += 1;
            }
        } else if (this.radius > this.minRadius) {
            this.radius -= 1;
        }

        this.draw();

    }

}




var circleArray = [];

function init() {
    circleArray = [];
    for (var i = 0; i < 1000; i++) {
        var radius = Math.random() * 3 + 1;
        var x = Math.random() * (innerWidth - radius * 2) + radius;
        var y = Math.random() * (innerHeight - radius * 2) + radius;
        var dx = (Math.random() - 0.5);
        var dy = (Math.random() - 0.5);
        circleArray.push(new Circle(x, y, dx, dy, radius));
    }
}



function type(text, font, x, y, fillStyle) {
    c.font = font;
    c.textAlign = 'center';
    c.fillStyle = fillStyle;
    c.fillText(text, x, y);
}

function animate() {
    requestAnimationFrame(animate);
    c.clearRect(0, 0, innerWidth, innerHeight);
    for (var i = 0; i < circleArray.length; i++) {
        circleArray[i].update();
    }
    type('Hello, user!', '50px Courier New', canvas.width / 2, 50, '#004A80');
    type('Are you ready to start the journey?', '50px Courier New', canvas.width / 2, 100, '#4DB5FF');
    type('Sign up and let the ', '25px Courier New', canvas.width / 2 - 100, 150, '#0095FF');
    type('SentimentPredator ', 'bold 40px Courier New', canvas.width / 2 + 260, 150, '#265A80');
    type('catch the feelings', 'bold 70px Courier New', canvas.width / 2 + 100, 220, '#0077CC');

    var w = 200, h = 200;
    c.strokeStyle = "lightblue";
    c.strokeWeight = 3;
    c.shadowOffsetX = 4.0;
    c.shadowOffsetY = 4.0;
    c.lineWidth = 10.0;
    c.fillStyle = "lightblue";
    var d = Math.min(w, h);
    var k = 120;
    c.moveTo(k, k + d / 4);
    c.quadraticCurveTo(k, k, k + d / 4, k);
    c.quadraticCurveTo(k + d / 2, k, k + d / 2, k + d / 4);
    c.quadraticCurveTo(k + d / 2, k, k + d * 3 / 4, k);
    c.quadraticCurveTo(k + d, k, k + d, k + d / 4);
    c.quadraticCurveTo(k + d, k + d / 2, k + d * 3 / 4, k + d * 3 / 4);
    c.lineTo(k + d / 2, k + d);
    c.lineTo(k + d / 4, k + d * 3 / 4);
    c.quadraticCurveTo(k, k + d / 2, k, k + d / 4);
    c.stroke();
}



init();
animate();


