﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication4.Models;

namespace WebApplication4.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tweet> Tweets { get; set; }
        public DbSet<Keyword_Tweet> Keyword_Tweets { get; set; }
    }
}
