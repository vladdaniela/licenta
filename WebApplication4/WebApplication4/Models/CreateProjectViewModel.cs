﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class CreateProjectViewModel
    {


        [Required(ErrorMessage = "Please enter a project name")]
        [Display(Name = "Project name")]
        public string Name { get; set; }

        public string UserId { get; set; }

    }
}
