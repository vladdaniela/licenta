from __future__ import absolute_import, division, print_function
import tensorflow as tf
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.linear_model import  SGDClassifier, LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import  SVC
from sklearn.pipeline import Pipeline
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
import os
import re
import string
import pickle
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tf.compat.v1.enable_eager_execution()

def preporcessor(text):
    new_vector = []
    for val in text:
        val_ = re.sub(r"http\S+", '', val)
        val_ = re.sub(r"\s+[a-zA-Z]\s+", ' ', val_)                             # remove all single characters
        val_ = re.sub(r"\^[a-zA-Z]\s+", ' ', val_)                              # Remove single characters from the start
        val_ = re.sub(r"&gt;", '', val_)
        val_ = re.sub(r"&amp;", '', val_)
        val_ = re.sub(r"&lt;3", '', val_)
        val_ = re.sub(r"quot;", '', val_)
        val_ = re.sub(r"VIDEO", '', val_)
        val_ = re.sub(r"--", '', val_)
        val_ = re.sub(r"---", '', val_)
        val_ = re.sub(r"RT", '', val_)
        val_ = val_.lower()
        new_vector.append(val_)
    return new_vector

# used to chop off the ends of words
def get_stemmed_text(corpus):
    from nltk.stem.snowball import SnowballStemmer
    stemmer = SnowballStemmer("romanian", ignore_stopwords=True)
    return [' '.join([stemmer.stem(word) for word in tweet.split()]) for tweet in corpus]


column_names = ['Text', 'Polarity']
dataset = pd.read_csv("Tweets_csv.csv", usecols=column_names)
#print(dataset)

X = dataset.iloc[:, 0].values#text
Y = dataset.iloc[:, 1].values#polarity

X = preporcessor(X)
X = get_stemmed_text(X)

stop = stopwords.words('romanian') + list(string.punctuation)
stop.append("printr")
print(stop)

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
vectorizer = CountVectorizer(max_features=1500, min_df=5, max_df=0.7,stop_words=stop, ngram_range=(1, 2))   #https://stackabuse.com/text-classification-with-python-and-scikit-learn/
X = vectorizer.fit_transform(X)
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)



# https://stackabuse.com/text-classification-with-python-and-scikit-learn/ =====> 56% accuracy
# classifier = RandomForestClassifier(n_estimators=100, random_state=0)
# classifier.fit(X_train, y_train)
# y_pred = classifier.predict(X_test)
# print(confusion_matrix(y_test, y_pred))
# print(classification_report(y_test, y_pred))
# print(accuracy_score(y_test, y_pred))


# https://towardsdatascience.com/machine-learning-nlp-text-classification-using-scikit-learn-python-and-nltk-c52b92a7c73a ====> 67%
# classifier = MultinomialNB().fit(X_train, y_train)
# y_pred = classifier.predict(X_test)
# print(confusion_matrix(y_test, y_pred))
# print(classification_report(y_test, y_pred))
# print(accuracy_score(y_test, y_pred))



# SVM ====> 66%
classifier = Pipeline([('tfidf', TfidfTransformer(use_idf=True)),
                        ('clf-svm', SGDClassifier(loss='hinge', penalty='l2', l1_ratio=0,
                                                  alpha=1e-3, max_iter=20, tol=None, random_state=0)),
                         ])
classifier = classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))
print(accuracy_score(y_test, y_pred))

params = {'clf-svm__loss' : ['hinge','modified_huber', 'log'],
          'clf-svm__penalty' : ['none', 'l2', 'l1', 'elasticnet'],
          # 'clf-svm__alpha' : [10 ** x for x in range(-6, 1)],
          'clf-svm__alpha': (1e-2, 1e-3),
          'clf-svm__max_iter':(20,),
          'clf-svm__tol':[None],
          # 'clf-svm__n_iter':range(0,10),
          'clf-svm__random_state': (0,1000),
          'tfidf__use_idf':(True, False),
          'clf-svm__l1_ratio': [0, 0.05, 0.1, 0.2, 0.5, 0.8, 0.9, 0.95, 1]
         }
print(classifier.get_params().keys())

grid_search = GridSearchCV(classifier, params, scoring='accuracy', cv=5)
grid_search.fit(X_train, y_train)

print("###########")
print(grid_search.best_params_)
print('Best score: ', grid_search.best_score_)
print('Actual score: ',grid_search.score(X_test,y_test))
print(accuracy_score(y_test, y_pred))
# print(grid_search.cv_results_)
print("###########")

#grid search with count vectorizer
# parameters = {
#     'vect__max_df': (0.5, 0.75, 1.0),
#     # 'vect__max_features': (None, 5000, 10000, 50000),
#     'vect__ngram_range': ((1, 1), (1, 2)),  # unigrams or bigrams
#     # 'tfidf__use_idf': (True, False),
#     # 'tfidf__norm': ('l1', 'l2'),
#     'clf__max_iter': (20,),
#     'clf__alpha': (0.00001, 0.000001),
#     'clf__penalty': ('l2', 'elasticnet'),
#     # 'clf__max_iter': (10, 50, 80),
# }
# pipeline = Pipeline([
#     ('vect', CountVectorizer()),
#     ('tfidf', TfidfTransformer()),
#     ('clf', SGDClassifier()),
# ])
# pipeline = pipeline.fit(X_train, y_train)
# y_pred = pipeline.predict(X_test)
# grid_search = GridSearchCV(pipeline, parameters, n_jobs=-1, verbose=1)
# grid_search.fit(X_train, y_train)
# print("###########")
# print(grid_search.best_params_)
# print('Best score: ', grid_search.best_score_)
# print('Actual score: ',grid_search.score(X_test,y_test))
# print(accuracy_score(y_test, y_pred))
#
# # print(grid_search.cv_results_)
# print("###########")


# Make predictions on validation dataset ====> 62%
# classifier = BernoulliNB()
# classifier = classifier.fit(X_train, y_train)
# predictions = classifier.predict(X_test)
# print(confusion_matrix(y_test, predictions))
# print(classification_report(y_test, predictions))
# print(accuracy_score(y_test, predictions))


# Logistic Regression =====> 55%
# classifier = LogisticRegression(C=0.5)
# classifier.fit(X_train, y_train)
# print("Final Accuracy: %s" % accuracy_score(y_test, classifier.predict(X_test)))


# Saving the model
# with open('text_classifier', 'wb') as picklefile:
#     pickle.dump(classifier, picklefile)

# # # Loading the model
with open('text_classifier', 'rb') as training_model:
    model = pickle.load(training_model)


def prepare(text):
    text = [text]
    [x.lower() for x in text]
    return vectorizer.transform(text)


print("Final Accuracy: %s" % accuracy_score(y_test, model.predict(X_test)))
#
print(model.predict(prepare("Norvegia este o tara super frumoasa! Recomand de vizitat!")))
print(model.predict(prepare("Accident teribil s-a petrecut ieri in Italia")))
print(model.predict(prepare("ce primavara frumoasa am avut!")))
print(model.predict(prepare("ieri am jucat fotbal in parc")))



from flask import request
import flask

# code which helps initialize our server
app = flask.Flask(__name__)

array_sentiments = ["neutral", "positive", "negative"]


@app.route('/predict', methods=['POST'])
def predict():
    text = request.get_json()['text']
    if text == "":
        return format(0)
    text = prepare(text)
    response = model.predict(text)
    return format(response[0])


if __name__ == '__main__':
    app.run(port=5003)


